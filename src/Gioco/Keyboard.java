package Gioco;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

	@Override
	public void keyPressed(KeyEvent e) {
		
		if(Main.scene.mario.isVivo() == true){
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			
				// per non fare muovere il castello e start 
				if(Main.scene.getXPosition() == -1){
					Main.scene.setXPosition(0);
					Main.scene.setX1(-50);
					Main.scene.setX2(750);
				}
				Main.scene.mario.setMoving(true);
				Main.scene.mario.setVerso_destra(true);
				Main.scene.setMov(1); // si muove verso sinistra
			}else if(e.getKeyCode() == KeyEvent.VK_LEFT){
			
				if(Main.scene.getXPosition() == 4601){
					Main.scene.setXPosition(4600);
					Main.scene.setX1(-50);
					Main.scene.setX2(750);
				}
			
				Main.scene.mario.setMoving(true);
				Main.scene.mario.setVerso_destra(false);
				Main.scene.setMov(-1); // si muove verso destra 
			}
			// salto
			if(e.getKeyCode() == KeyEvent.VK_UP){
				Main.scene.mario.setSalto(true);
				Audio.playSound("/audio/jump.wav");
			}
			
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Main.scene.mario.setMoving(false);
		Main.scene.setMov(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

}
